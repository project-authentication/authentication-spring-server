package com.authentication.springserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthenticationSpringServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthenticationSpringServerApplication.class, args);
    }

}
