# Authorization Server
Ejecutar projecto Spring Boot, la aplicación corre en el puerto `:9000`

Para ver los puntos de entrada `end points` abra en un navegador http://localhost:9000/.well-known/oauth-authorization-server

### 1.- Basic authorization
Ingresar a la página https://oauthdebugger.com/debug y completar con los siguientes datos:

|   |                                                       | 
|---|-------------------------------------------------------|
|Authorize URI|http://localhost:9000/oauth2/authorize|
|Redirect URI|https://oauthdebugger.com/debug|
|Client ID|oidc-client|
|Scope|openid|
|Response type| seleccionar `code` y en Use PKCE? seleccionar SHA-256|
|Response mode| seleccionar `form_post`|

Pulsar `SEND REQUEST`, si todo es correcto nos mostrará una ventana de `Success!` y el `authorization code` 
que nos servirá para obtener el token.

#### Obteniendo el token
Se solicitará el token usan una peticion HTTP usando Postman o herramientas similares.
```
curl --request POST \
  --url http://localhost:9000/oauth2/token \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data grant_type=authorization_code \
  --data client_id=oidc-client \
  --data redirect_uri=https://oauthdebugger.com/debug \
  --data code_verifier=[Code Verifier] \
  --data code=[authorization code]
```
Ademas antes de enviar de debe enviar acompañado de un `Basic Auth` con el usuario `oidc-client` y contraseña `password`.

Si todo fue correcto nos dara una respuesta con el `access_token`, `refresh_token`, con el que podremos autenticarnos en el sistema.